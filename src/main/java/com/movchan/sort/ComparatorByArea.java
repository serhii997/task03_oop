package com.movchan.sort;

import com.movchan.model.Dwelling;

import java.util.Comparator;

public class ComparatorByArea implements Comparator {
    public int compare(Object o1, Object o2) {
        return (int) (((Dwelling) o1).getArea() - ((Dwelling) o2).getArea());
    }
}
