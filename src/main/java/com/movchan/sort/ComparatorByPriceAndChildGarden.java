package com.movchan.sort;

import com.movchan.model.Dwelling;

import java.util.Comparator;

public class ComparatorByPriceAndChildGarden implements Comparator <Dwelling> {
    public int compare(Dwelling o1, Dwelling o2) {
        int temp = ( o1).getPrice() - ( o2).getPrice();
        if (temp == 0) {
            return (o1).getChildGardenLocation() - (o2).getChildGardenLocation();
        }
        return temp;
    }

}
