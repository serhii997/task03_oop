package com.movchan.sort;

import com.movchan.model.Dwelling;

import java.util.Comparator;

public class ComparatorByPrice implements Comparator {
    public int compare(Object o1, Object o2) {
        return ((Dwelling)o1).getPrice() - ((Dwelling)o2).getPrice();
    }
}
