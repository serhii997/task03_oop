package com.movchan.controller;

public interface Controller {
    void printList();
    void sortByPrice();
    void sortByArea();
    void sortByPriceAndChildGarden();
    void sortByPriceAndSchool();
    void sortByPriceAndPlayground();
    void filterByPrice(int price);
}
