package com.movchan.controller;

import com.movchan.model.BuisnessLogic;
import com.movchan.model.Dwelling;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Controller Realisation.</h1>
 * This class implements interface Controller and your method
 * simply displays work with this class.
 * Print result, sort and filter by dwelling.
 *
 * @author  Serhii Movchan
 * @version 1.0
 * @since   2019-11-08
 */

public class ControllerRealisation implements Controller {
    /** field save all dwelling.*/
    private List<Dwelling> dwellings;
    private BuisnessLogic buisnessLogic;

    /**
     * This constructor create dwelling list.
     * This is a the simplest form of a class constructor.
     */
    public ControllerRealisation() {
        dwellings = new ArrayList<>();
        buisnessLogic = new BuisnessLogic();
    }

    /**
     * This method output dwelling list.
     * This is a the simplest form of a class method.
     */
    public void printList() {
      buisnessLogic.printList();
    }

    public void sortByPrice() { buisnessLogic.sortByPrice(); }

    public void sortByArea() { buisnessLogic.sortByArea(); }

    public void sortByPriceAndChildGarden() {
        buisnessLogic.sortByPriceAndChildGarden();
    }

    public void sortByPriceAndSchool() { buisnessLogic.sortByPriceAndSchool(); }

    public void sortByPriceAndPlayground() { buisnessLogic.sortByPriceAndPlayground(); }

    public void filterByPrice(int price) { buisnessLogic.filterByPrice(price).forEach(System.out::println); }
}
