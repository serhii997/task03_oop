package com.movchan.view;

import com.movchan.controller.Controller;
import com.movchan.controller.ControllerRealisation;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * <h1>CDwelling List.</h1>
 * This class implements menu and
 * simply displays work with this class.
 * Print result, sort and filter by dwelling.
 *
 * @author  Serhii Movchan
 * @version 1.0
 * @since   2019-11-08
 */

public class DwellingList {
    /** field create Controller.*/
    private Controller controller;
    /** field create menu list.*/
    private Map<String, String> menu;
    /** field create function menu.*/
    private Map<String, Printable> functionMenu;

    /** field create Scanner for input .*/
    private static Scanner sc = new Scanner(System.in);
    /**
     * This constructor create date for menu.
     * This is a the simplest form of a class constructor.
     */
    public DwellingList() {
        controller = new ControllerRealisation();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "   1 - print dwelling list");
        menu.put("2", "   2 - sort by price");
        menu.put("3", "   3 - sort by area");
        menu.put("4", "   4 - sort by price and child garden location");
        menu.put("5", "   5 - sort by price and school");
        menu.put("6", "   6 - sort by price and playground");
        menu.put("7", "   7 - filter by price");
        menu.put("0", "   0 - exit");

        functionMenu = new LinkedHashMap<String, Printable>();

        functionMenu.put("1", this::printList);
        functionMenu.put("2", this::sortByPrice);
        functionMenu.put("3", this::sortByArea);
        functionMenu.put("4", this::sortByPriceAndChildGarden);
        functionMenu.put("5", this::sortByPriceAndSchool);
        functionMenu.put("6", this::sortByPriceAndPlayground);
        functionMenu.put("7", this::filter);
    }
    /**
     * This method implement function for 1 button.
     * This is a the simplest form of a class method.
     */
    private void printList(){
        controller.printList();
    }
    /**
     * This method implement function for 2 button.
     * This is a the simplest form of a class method.
     */
    private void sortByPrice() {
        controller.sortByPrice();
        controller.printList();
    }
    /**
     * This method implement function for 3 button.
     * This is a the simplest form of a class method.
     */
    private void sortByArea() {
        controller.sortByArea();
        controller.printList();
    }
    /**
     * This method implement function for 4 button.
     * This is a the simplest form of a class method.
     */
    private void sortByPriceAndChildGarden() {
        controller.sortByPriceAndChildGarden();
        controller.printList();
    }
    /**
     * This method implement function for 5 button.
     * This is a the simplest form of a class method.
     */
    private void sortByPriceAndSchool() {
        controller.sortByPriceAndSchool();
        controller.printList();
    }
    /**
     * This method implement function for 6 button.
     * This is a the simplest form of a class method.
     */
    private void sortByPriceAndPlayground() {
        controller.sortByPriceAndPlayground();
        controller.printList();
    }
    /**
     * This method implement function for 7 button.
     * Print filter which a less than 5000
     * This is a the simplest form of a class method.
     */
    private void filter() {
        System.out.println("Pl enter price");
        controller.filterByPrice(sc.nextInt());
    }
    /**
     * This privet method print Menu.
     * This is a the simplest form of a class method.
     */
    private void printMenu() {
        System.out.println("\n Menu:");
        for (String s: menu.values()) {
            System.out.println(s);
        }
    }
    /**
     * This method implement function for show menu.
     * This is a the simplest form of a class method.
     */
    public void showMenu() {
        String keyMenu;
        do{
            System.out.println("Pl, enter menu point");
            printMenu();
            keyMenu = sc.nextLine().toLowerCase();
            try{
                functionMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println("Pl enter correct number");
            }
        }while (!keyMenu.equals(0));

    }
}
