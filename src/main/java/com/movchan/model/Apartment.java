package com.movchan.model;

public class Apartment extends Dwelling {
    private int floor;

    public Apartment(String name, int price, double area, int floor) {
        super(name, price, area);
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Apartment{" + super.toString()+
                "floor=" + floor +
                '}';
    }
}

