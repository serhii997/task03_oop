package com.movchan.model;

import java.util.Random;

public class Dwelling {
    private String name;
    private int price;
    private double area;
    private int childGardenLocation;
    private int schoolLocation;
    private int playgroundLocation;

    public Dwelling() {
    }

    public Dwelling(String name, int price, double area) {
        this.name = name;
        this.price = price;
        this.area = area;

        fillSocialObject();
    }

    private void fillSocialObject() {
        final int LOCATION = 1000;
        Random random = new Random();
        childGardenLocation = random.nextInt(LOCATION);
        schoolLocation = random.nextInt(LOCATION);
        playgroundLocation = random.nextInt(LOCATION);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public int getChildGardenLocation() {
        return childGardenLocation;
    }

    public void setChildGardenLocation(int childGardenLocation) {
        this.childGardenLocation = childGardenLocation;
    }

    public int getSchoolLocation() {
        return schoolLocation;
    }

    public void setSchoolLocation(int schoolLocation) {
        this.schoolLocation = schoolLocation;
    }

    public int getPlaygroundLocation() {
        return playgroundLocation;
    }

    public void setPlaygroundLocation(int playgroundLocation) {
        this.playgroundLocation = playgroundLocation;
    }

    @Override
    public String toString() {
        return "Dwelling{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", area=" + area +
                ", childGardenLocation=" + childGardenLocation +
                ", schoolLocation=" + schoolLocation +
                ", playgroundLocation=" + playgroundLocation +
                '}';
    }

}
