package com.movchan.model;

import com.movchan.sort.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BuisnessLogic implements Model {

    private List<Dwelling> dwellings;

    /**
     * This constructor create dwelling list.
     * This is a the simplest form of a class constructor.
     */
    public BuisnessLogic() {
        dwellings = new ArrayList<>();
        addDwelling();
    }
    /**
     * This method adding and save date on list and output of Fibonacci numbers on range.
     * This is a the simplest form of a class method.
     */
    private void addDwelling(){
        dwellings = Arrays.asList(
                new Apartment("Avalon",4000,60, 5),
                new Dwelling("Smetana",10000,180),
                new Penthouse("Parus",8000,100, 7),
                new Apartment("Parus",4000,75, 5),
                new Penthouse("Green Hill",4100,75, 5),
                new Mansion("Sokilnuki",10000,200),
                new Dwelling("Avalon",10000,60)
        );

    }
    /**
     * This method output dwelling list.
     */
    @Override
    public void printList() {
        for (Dwelling dwelling: dwellings) {
            System.out.println(dwelling);
        }
    }
    /**
     * This method sort dwellings list by price.
     */
    @Override
    public void sortByPrice() {
        Collections.sort(dwellings, new ComparatorByPrice());
    }
    /**
     * This method sort dwellings list by area.
     */
    @Override
    public void sortByArea() {
        Collections.sort(dwellings, new ComparatorByArea());
    }
    /**
     * This method sort dwellings list by price and child garden location.
     */
    @Override
    public void sortByPriceAndChildGarden() {
        Collections.sort(dwellings, new ComparatorByPriceAndChildGarden());
    }
    /**
     * This method sort dwellings list by price and school.
     */
    @Override
    public void sortByPriceAndSchool() { Collections.sort(dwellings, new ComparatorByPriceAndSchool()); }
    /**
     * This method sort dwellings list by price and playground location.
     */
    @Override
    public void sortByPriceAndPlayground() {
        Collections.sort(dwellings, new ComparatorByPriceAndPlayground());
    }

    /**
     * dwelling list apartments by price.
     * @param price max shape
     * @return dwelling list
     */
    @Override
    public List<Dwelling> filterByPrice(int price) {
        return dwellings.stream()
                .filter(dwelling -> price >= dwelling.getPrice())
                .collect(Collectors.toList());
    }
}
