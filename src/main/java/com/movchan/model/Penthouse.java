package com.movchan.model;

public class Penthouse extends Dwelling {
    private int floor;

    public Penthouse(String name, int price, double area, int floor) {
        super(name, price, area);
        this.floor = floor;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Penthouse{" + super.toString()+
                "floor=" + floor +
                '}';
    }
}
