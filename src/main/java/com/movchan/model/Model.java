package com.movchan.model;

import java.util.List;

public interface Model {

    void printList();
    void sortByPrice();
    void sortByArea();
    void sortByPriceAndChildGarden();
    void sortByPriceAndSchool();
    void sortByPriceAndPlayground();
    List<Dwelling> filterByPrice(int price);

}
