package com.movchan.model;

public class Mansion extends Dwelling {
    public Mansion(String name, int price, double area) {
        super(name, price, area);
    }
}
